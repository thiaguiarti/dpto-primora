# DPTO Primora

## Guia rápido

* Clone ou baixe este repositório Git em seu computador.
* Instale o [Node.js](https://nodejs.org/en/) se ainda não o tiver.
* Execute `npm install` e `npm install gulp-cli`.
* Execute `gulp` para executar a tarefa Gulp padrão.
* Obervações: O comando `gulp` já cria a pasta build, gerando os arquivos finais compilados. A pasta node_modules e build ficam ocultas no .gitignore.
