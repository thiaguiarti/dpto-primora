// bootstrap poppovers
$(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
});

// modal
$('.modal').modal(
    {
        backdrop: 'static',
        keyboard: false,
        show: false
    }
);

$(function () {
    $(document).scroll(function () {
        var $nav = $("header");
        $nav.toggleClass('scrolled', $(this).scrollTop() > $nav.height());
    });

    $(".sidebarIconToggle").on(
        "click", function() {
            $("header").toggleClass('actived');
            $(".menu").toggle();
        }
    );
    
    $("#itemportfolio").on(
        "click", function() {
            $("#itemportfolio").toggleClass("open");
            if($(this).hasClass('open')) {
                $("#submenu").show();
            } else {
                $("#submenu").hide();
            }
        }
    );
    
    $("body").on(
        "click", function(e) {
            if (e.target.id !== "itemportfolio" && $(e.target).parents("#itemportfolio").length === 0) {
                $("#submenu").hide();
                $("#itemportfolio").removeClass('open');
            }
        }
    );
});